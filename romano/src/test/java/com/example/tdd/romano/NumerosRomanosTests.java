package com.example.tdd.romano;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class NumerosRomanosTests {

    @BeforeEach
    public void inicializacao(){

    }

    List<Integer> lstNumerosRomanos = Arrays.asList(1,4,5,8,10,21,50,90,100,500,511,1000,3001);
    List<String> lstAlgarismoRomanos = Arrays.asList("I","IV","V","VIII","X","XXI","L","XC","C","D","DXI","M","MMMI");
    @Test
    public void testarConversorDeNumero() throws Exception{

         String algarismo;
        ConversorNumeroRomano conversorNumeroRomano  = new ConversorNumeroRomano();
        try {
            for (int t = 0; t < lstNumerosRomanos.size(); t++) {

                algarismo = conversorNumeroRomano.conversorDeNumero((Integer) lstNumerosRomanos.get(t));
                Assertions.assertEquals(algarismo, lstAlgarismoRomanos.get(t));
            }
        } catch (Exception ex)
        {

            Assertions.assertEquals(ex.getMessage(),"O Programa esta limitando a conversão para romano em 3000");
        }

        }

    @Test
    public void testarConversorDeNumeroZero() throws Exception {


        ConversorNumeroRomano conversorNumeroRomano = new ConversorNumeroRomano();
        try {
            conversorNumeroRomano.conversorDeNumero(0);

        } catch (Exception ex) {
            Assertions.assertEquals(ex.getMessage(), "Valor não pode ser Zero");

        }
    }
    @Test
    public void testarConversorDeNumeroLimite() throws Exception{


        ConversorNumeroRomano conversorNumeroRomano  = new ConversorNumeroRomano();
        try {
            conversorNumeroRomano.conversorDeNumero(3001);

        } catch (Exception ex)
        {
            Assertions.assertEquals(ex.getMessage(),"O Programa esta limitando a conversão para romano em 3000");

        }

    }


}
