package com.example.tdd.romano;

public class ConversorNumeroRomano {

    private final String ROMANO_I = "I";
    private final String ROMANO_V = "V";
    private final String ROMANO_X = "X";
    private final String ROMANO_L = "L";
    private final String ROMANO_C = "C";
    private final String ROMANO_D = "D";
    private final String ROMANO_M = "M";

    public String conversorDeNumero(Integer numeroDec) throws Exception{

        if (numeroDec==0)
             throw new Exception("Valor não pode ser Zero");

        if(numeroDec>=3000)
            throw new Exception("O Programa esta limitando a conversão para romano em 3000");

        String resultado = "";
        String nCaracter = String.valueOf(numeroDec);
        Integer[] posicao = {0,0,0,0};

        for (int c = 0 ; c < nCaracter.length() ; c++)
        {
            switch (c) {
                case 0:
                    posicao[0] = Integer.parseInt(nCaracter.substring(0, 1));
                    resultado = retornaP1(posicao[0]);
                    break;
                case 1:
                    posicao[0] = Integer.parseInt(nCaracter.substring(0, 1));
                    posicao[1] = Integer.parseInt(nCaracter.substring(1, 2));
                    resultado = retornaP2(posicao[0]) + retornaP1(posicao[1]);
                    break;
                case 2:
                    posicao[0] = Integer.parseInt(nCaracter.substring(0, 1));
                    posicao[1] = Integer.parseInt(nCaracter.substring(1, 2));
                    posicao[2] = Integer.parseInt(nCaracter.substring(2, 3));
                    resultado = retornaP3(posicao[0]) + retornaP2(posicao[1]) + retornaP1(posicao[2]);
                    break;
                case 3:
                    posicao[0] = Integer.parseInt(nCaracter.substring(0, 1));
                    posicao[1] = Integer.parseInt(nCaracter.substring(1, 2));
                    posicao[2] = Integer.parseInt(nCaracter.substring(2, 3));
                    posicao[3] = Integer.parseInt(nCaracter.substring(3, 4));
                    resultado = retornaP4(posicao[0]) + retornaP3(posicao[1]) + retornaP2(posicao[2]) + retornaP1(posicao[3]);

            }

        }
    //    System.out.println("entrada : " + numeroDec + " saida " + resultado);
        return resultado;
    }

    public String retornaP1(int numero) {
        String numRomano = new String();
        if (numero <= 3) {
            for (int i = 0; i < numero; i++) {
                if (numero > i) {
                    numRomano += ROMANO_I;
                }
            }
        } else if (numero >= 5 && numero < 9) {
            numRomano = ROMANO_V;
            if (numero > 5) {
                for (int i = 5; i < numero; i++) {
                    if (numero > i) {
                        numRomano += ROMANO_I;
                    }
                }
            }
        } else if (numero == 4) {
            numRomano =  ROMANO_I+ROMANO_V;
        } else {
            numRomano = ROMANO_I+ROMANO_X;
        }
        return numRomano;
    }

    public String retornaP2(int numero) {
        String numRomano = new String();
        if (numero <= 3) {
            for (int i = 0; i < numero; i++) {
                if (numero > i) {
                    numRomano += ROMANO_X;
                }
            }
        } else if (numero >= 5 && numero < 9) {
            if (numero == 5) {
                numRomano = ROMANO_L;
            } else {
                numRomano = ROMANO_L;
                for (int i = 5; i < numero; i++) {
                    if (numero > i) {
                        numRomano += ROMANO_X;
                    }
                }
            }
        } else if (numero == 4) {
            numRomano = ROMANO_X+ROMANO_D;
        } else {
            numRomano = ROMANO_X+ROMANO_C;
        }
        return numRomano;
    }

    public String retornaP3(int numero) {
        String numRomano = new String();
        if (numero <= 3) {
            for (int i = 0; i < numero; i++) {
                if (numero > i) {
                    numRomano +=ROMANO_C;
                }
            }
        } else if (numero >= 5 && numero < 9) {
            if (numero == 5) {
                numRomano = ROMANO_D;
            } else {
                numRomano = ROMANO_D;
                for (int i = 5; i < numero; i++) {
                    if (numero > i) {
                        numRomano += ROMANO_M;
                    }
                }
            }
        } else if (numero == 4) {
            numRomano = ROMANO_C+ROMANO_D;
        } else {
            numRomano = ROMANO_D+ROMANO_M;
        }
        return numRomano;
    }

    public String retornaP4(int numero) {
        String numRomano = new String();
        if (numero <= 3) {
            for (int i = 0; i < numero; i++) {
                if (numero > i) {
                    numRomano += ROMANO_M;
                }
            }
        }
        return numRomano;
    }

}
